# WitClient
Programma scritto in Go per facilitare l'elaborazione degli audio per il corso 
di Linguistica Italiana II dell'Università di Pisa. Sono disponibili 
due binari ([uno](https://gitlab.com/levnikmyskin/witclient/raw/master/WitClient) per Linux e [uno](https://gitlab.com/levnikmyskin/witclient/raw/master/WitClient.exe) 
per Windows). È sufficiente scaricare quello corretto per il proprio sistema operativo
ed eseguirlo da terminale. **Se si desidera tagliare gli audio sulle pause, è necessario 
avere installato [SoX](http://sox.sourceforge.net/) sul proprio sistema**.  
NB: il programma è stato testato per lo più su un sistema Linux, ma dovrebbe funzionare senza problemi anche 
su Windows

## Preparazione all'utilizzo 
Il programma può tagliare gli audio sulle pause in automatico, tramite l'utilizzo di SoX (da installare se non già presente 
sul sistema). I file risultanti saranno poi inviati alle API di Wit e di Google Cloud. È possibile decidere di non tagliare 
gli audio o di usare uno solo dei due servizi Speech to Text (o addirittura nessuno dei due, utile per tagliare gli audio 
senza inviarli).  

Per utilizzare Google, è necessario posizionare il file `googlespeech.json` nella stessa cartella in cui si trova l'eseguibile 
del programma. Si veda [qui](https://cloud.google.com/speech-to-text/docs/quickstart-client-libraries) il passaggio 1 della
sezione **Before you begin**.  

Un passaggio simile è richiesto per usare Wit, si veda [qui](https://wit.ai/docs/quickstart).

## Utilizzo
Per utilizzare il programma, dirigersi con il terminale nella cartella in cui è presente l'eseguibile, dopodiché eseguire 
il programma passando le opzioni necessarie, esempio per dividere un file wav con SoX e inviarlo poi sia a Wit che Google,
utilizzare:  
`./WitClient -input LMp1A02R-1.wav -format wav -use-google `  
  
È possibile visualizzare un elenco completo delle opzioni utilizzando `./WitClient -help`; l'elencazione segue questo formato:  
  ```
  -nomeopzione tipo
    spiegazione
   ```
dove il `tipo` indica il tipo di valore da immettere:  
* `string` indica un valore testuale, es. `-format wav`;  
* `int` indica un numero intero, es. `-ignore-size 30`;
* `float` indica un numero decimale, es. `threshold 1.3`;  
* Se vuoto significa che quell'opzione, se passata, viene "attivata" (diventa vera), es.
`-use-google` indica che verrà usato Google. Oppure `-no-split` indica che 
non verranno divisi i file audio.  

Le opzioni nella categoria "avanzate" sono opzioni che vengono passate direttamente a SoX per 
un controllo più granulare della divisione degli audio. Per maggior informazioni vedere [qui](https://digitalcardboard.com/blog/2009/08/25/the-sox-of-silence/comment-page-2/).
