package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"time"
	speech "cloud.google.com/go/speech/apiv1"
	speechpb "google.golang.org/genproto/googleapis/cloud/speech/v1"

)

const witTokenJson = "wittoken.json"
const googleTokenJson = "googlespeech.json"
const witApiEndpoint = "https://api.wit.ai/speech?v=20170307"
const WitSttOutputFile = "wit-trascrizioni.txt"
const GoogleSttOutputFile = "google-trascrizioni.txt"


var (
	inputFileName string
	audioFormat string
	witToken string
	useGoogle bool
	noWit bool
	noSplit bool
	splitOnly bool
	keepPause bool
	beforeSplit bool
	afterSplit bool
	threshold float64
	pauseDurationBefore float64
	pauseDurationAfter float64
	ignoreSize int
)

type WitResponse struct {
	MsgId string `json:"msg_id"`
	Text string `json:"_text"`
	Entities interface{} `json:"entities"`
}

func main() {
	var err error
	flag.StringVar(&inputFileName, "input", "", "Il file audio di input")
	flag.StringVar(&audioFormat, "format", "", "Il formato del file audio, sono supportati solo i" +
		" seguenti: wav, mp3, ogg, ulaw, raw")
	flag.StringVar(&witToken, "token", "", "Il token da usare con wit (opzionale). Controlla qui se non sai " +
		"come ottenerlo https://wit.ai/docs/quickstart")
	flag.BoolVar(&useGoogle, "google", false, "Usa anche Google per le trascrizioni")
	flag.BoolVar(&noWit, "no-wit", false, "Non usare Wit per le trascrizioni")
	help := flag.Bool("help", false, "Stampa questo messaggio d'aiuto e esce")
	flag.BoolVar(&noSplit, "no-split", false, "Non dividere il file con SoX")
	flag.BoolVar(&splitOnly, "split-only", false, "Dividi il file senza inviarlo a Google e Wit")
	flag.BoolVar(&beforeSplit, "bef-split", false, "Considera pause all'inizio")
	flag.BoolVar(&afterSplit, "aft-split", false, "Considera pause alla fine")
	flag.Float64Var(&pauseDurationBefore, "before-duration", 0, "Durata pausa all'inizio")
	flag.Float64Var(&pauseDurationAfter, "after-duration", 1.25, "Durata pausa alla fine")
	flag.Float64Var(&threshold, "threshold", 1, "Soglia del rumore, percentuale (es. 1)")
	flag.BoolVar(&keepPause, "keep-pause", false, "Mantieni le pause dopo averle tagliate")
	flag.IntVar(&ignoreSize, "ignore-size", 40, "Ignora file più piccoli della cifra specificata (KB)")
	flag.Parse()

	if len(os.Args) == 1 {
		fmt.Println("Non sono stati forniti argomenti!")
		usage()
		os.Exit(1)
	}

	if *help {
		usage()
		os.Exit(0)
	}

	if audioFormat == "" {
		fmt.Println("Non è stato specificato il formato audio (wav, mp3, ogg, ulaw, raw)")
		time.Sleep(time.Second / 2)
		usage()
		os.Exit(1)
	}

	if !soxExist() && !noSplit{
		fmt.Println("Per eseguire il programma, è necessario avere installato Sox sul sistema. Se lo hai installato, " +
			"è possibile che non sia presente nell'ambiente (su Linux devi modificare la variabile PATH)")
		os.Exit(1)
	}

	if inputFileName == "" {
		fmt.Println("Non è stato specificato un file di input")
		time.Sleep(time.Second / 2)
		usage()
		os.Exit(1)
	}

	if len(witToken) == 0 && !noWit{
		if !witJsonExist() {
			witToken, err = askUserForWitToken()
			if err != nil {
				fmt.Println("C'è stato un errore nel salvare il file con il token :(")
				fmt.Println(err)
				os.Exit(1)
			}
		} else {
			witToken, err = retrieveTokenFromFile()
			if err != nil {
				fmt.Printf("C'è stato un errore nel leggere il token Wit dal file %s. Prova a cancellare il file\n",
					witTokenJson)
				os.Exit(1)
			}
		}
	}

	if !noSplit {
		stderr := &strings.Builder{}
		err = chunkAudioWithSox(stderr)
		if err != nil {
			fmt.Println("C'è stato un errore nell'elaborare l'audio tramite Sox")
			fmt.Println(stderr.String())
		}
	}

	if !splitOnly {
		err = os.Setenv("GOOGLE_APPLICATION_CREDENTIALS", googleTokenJson)
		if err != nil {
			fmt.Println(err)
		}
		doSpeechToText()
	}
}

func soxExist() bool {
	cmd := exec.Command("sox", "--help")
	err := cmd.Run()
	return err == nil
}

func witJsonExist() bool {
	_, err := os.Stat(witTokenJson)
	return err == nil
}

func askUserForWitToken() (string, error) {
	fmt.Println("Per funzionare, il programma necessita di un token di Wit. Per ottenerlo è necessario registrarsi" +
		" su Wit e creare un'applicazione. Puoi vedere come fare su questo link https://wit.ai/docs/quickstart")
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Inserisci il token di WIT:")
	token, err := reader.ReadString('\n')
	if err != nil {
		return "", err
	}
	token = strings.Trim(token, "\n")
	file, err := os.Create(witTokenJson)
	if err != nil {
		return "", err
	}
	defer file.Close()
	_, err = file.WriteString(fmt.Sprintf(`{"token": "%s"}`, token))
	return token, err
}

func retrieveTokenFromFile() (string, error) {
	var tokenMap map[string]string
	file, err := os.Open(witTokenJson)
	if err != nil {
		return "", err
	}
	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&tokenMap)
	return tokenMap["token"], err
}

func chunkAudioWithSox(stderr *strings.Builder) error {
	args := make([]string, 17)
	out := "out" + inputFileName

	thresholdStr := fmt.Sprintf("%.1f%s", threshold, "%")
	args[0] = inputFileName
	args[1] = out
	args[2] = "silence"
	if keepPause {
		args[3] = "-l"
	}
	if beforeSplit {
		args[4] = "1"
		args[5] = fmt.Sprintf("%.1f", pauseDurationBefore)
		args[6] = thresholdStr
	} else {
		args[4] = "0"
	}

	if afterSplit {
		args[7] = "1"
		args[8] = fmt.Sprintf("%.1f", pauseDurationAfter)
		args[9] = thresholdStr
	} else {
		args[7] = "0"
	}

	args[10] = "pad"
	args[11] = "0"
	args[12] = "0.5"
	args[13] = ":"
	args[14] = "newfile"
	args[15] = ":"
	args[16] = "restart"

	cleanedArgs := cleanArgs(args)
	cmd := exec.Command("sox", cleanedArgs...)
	cmd.Stderr = stderr
	return cmd.Run()
}

func cleanArgs(args []string) []string {
	cleaned := make([]string, len(args))
	i := 0
	for _, el := range args {
		if el != "" {
			cleaned[i] = el
			i += 1
		}
	}
	return cleaned
}

func doSpeechToText() {
	doneChan := make(chan bool)
	toWait := 0
	witResp := &WitResponse{}
	files, err := filepath.Glob("out*")
	if err != nil {
		fmt.Println("C'è stato un errore nell'ottenere i file di output")
		fmt.Println(err)
		os.Exit(1)
	}
	client := &http.Client{}
	witOutputSTT, err := os.Create(WitSttOutputFile)
	if err != nil {
		fmt.Println("ERRORE: Non mi è stato possibile creare un file dove mettere le trascrizioni!")
		fmt.Println(err)
		os.Exit(1)
	}
	defer witOutputSTT.Close()

	googleOutputStt, err := os.Create(GoogleSttOutputFile)
	if err != nil {
		fmt.Println("ERRORE: Non mi è stato possibile creare un file dove mettere le trascrizioni!")
		fmt.Println(err)
		os.Exit(1)
	}
	defer googleOutputStt.Close()

	googleClient, err := speech.NewClient(context.Background())
	if err != nil {
		fmt.Println("ERRORE: Non è stato possibile creare un client Google")
		fmt.Println(err)
		os.Exit(1)
	}

	witTranscriptions := make([]string, len(files))
	googleTranscriptions := make([]string, len(files))

	for i, fileName := range files {
		toWait += 2
		go witSpeechToText(fileName, client, &witTranscriptions, i, witToken, witResp, doneChan)
		go googleSpeechToText(fileName, googleClient, &googleTranscriptions, i, doneChan)
	}

	for i := 0; i < toWait; i++ {
		<-doneChan
	}

	for i, trascr := range witTranscriptions {
		_, err := witOutputSTT.WriteString(trascr)
		if err != nil {
			fmt.Println("ERRORE: Ho ricevuto la trascrizione ma non mi è stata possibile scriverla sul file per Wit\n" +
				"Trascrizione: ", trascr)
		}

		_, err = googleOutputStt.WriteString(googleTranscriptions[i])
		if err != nil {
			fmt.Println("ERRORE: Ho ricevuto la trascrizione ma non mi è stata possibile scriverla sul file per Google\n" +
				"Trascrizione: ", trascr)
		}
	}

	fmt.Println("Tutti i file sono stati trascritti nei file " + WitSttOutputFile + ", " + GoogleSttOutputFile)
}

func googleSpeechToText(fileName string, client *speech.Client, transcriptions *[]string, index int, done chan bool) {
	if !isFileToBeSent(fileName) || !useGoogle{
		done <- true
		return
	}
	data, err := ioutil.ReadFile(fileName)
	if err != nil {
		fmt.Println("ERRORE: Impossibile elaborare il file " + fileName + ",non sarà disponibile una trascrizione")
		fmt.Println(err)
		done <- true
		return
	}
	resp, err := client.Recognize(context.Background(), &speechpb.RecognizeRequest{
		Config: &speechpb.RecognitionConfig{
			Encoding: speechpb.RecognitionConfig_LINEAR16,
			LanguageCode: "it-IT",
		},
		Audio: &speechpb.RecognitionAudio{
			AudioSource: &speechpb.RecognitionAudio_Content{Content: data},
		},
	})
	if err != nil {
		fmt.Println("ERRORE: Fallita l'elaborazione di Google per il file " + fileName + "! Non sarà disponibile una trascrizione")
		fmt.Println(err)
		done <- true
		return
	}
	builder := strings.Builder{}

	_, err = builder.WriteString("# File " + fileName + "\n")
	if err != nil {
		fmt.Println("ERRORE interno!")
		fmt.Println(err)
		done <- true
		return
	}
	for _, results := range resp.Results {
		_, err = builder.WriteString(results.Alternatives[0].Transcript + "\n\n")
		if err != nil {
			fmt.Println("ERRORE: Ho ricevuto la trascrizione per il file " + fileName + " ma non mi è stato possibile " +
				"scriverla internamente!!La trascrizione:\n" + results.Alternatives[0].Transcript)
			fmt.Println(err)
		}
	}
	(*transcriptions)[index] = builder.String()
	done <- true
}

func witSpeechToText(fileName string, client *http.Client, transcriptions *[]string, index int, witToken string, witResp *WitResponse, done chan bool) {
	if !isFileToBeSent(fileName) || noWit {
		done <- true
		return
	}
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("ERRORE: Impossibile elaborare il file " + fileName + ",non sarà disponibile una trascrizione")
		fmt.Println(err)
		done <- true
		return
	}
	req, err := http.NewRequest(http.MethodPost, witApiEndpoint, file)
	if err != nil {
		fmt.Println("ERRORE: Impossibile elaborare il file " + fileName + ",non sarà disponibile una trascrizione")
		fmt.Println(err)
		done <- true
		return
	}
	req.Header.Set("Content-Type", "audio/" + audioFormat)
	req.Header.Set("Authorization", "Bearer " + witToken)
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("ERRORE: Fallita l'elaborazione di Wit per il file " + fileName + "! Non sarà disponibile una trascrizione")
		fmt.Println(err)
		done <- true
		return
	}
	decoder := json.NewDecoder(resp.Body)
	err = decoder.Decode(witResp)
	if err != nil {
		fmt.Println("ERRORE: Fallita l'elaborazione di Wit per il file " + fileName + "! Non sarà disponibile una trascrizione")
		fmt.Println(err)
		done <- true
		return
	}
	builder := strings.Builder{}
	_, err = builder.WriteString("# File " + fileName + "\n" + witResp.Text + "\n\n")
	if err != nil {
		fmt.Println("Ho ricevuto la trascrizione per il file " + fileName + " ma non mi è stato possibile " +
			"scriverla internamente!!La trascrizione:\n" + witResp.Text)
		fmt.Println(err)
		done <- true
		return
	}
	(*transcriptions)[index] = builder.String()
	fmt.Println("Il file " + fileName + " è stato trascritto correttamente. Aspetto 1 secondo...")
	time.Sleep(time.Second)
	done <- true
}

func isFileToBeSent(fileName string) bool {
	stat, err := os.Stat(fileName)
	if err != nil {
		fmt.Println(err)
		return false
	}
	// process files bigger than ignoreSize only
	if int(stat.Size() / 1000) < ignoreSize {
		fmt.Printf("Ignoro file %s perché più piccolo di %d KB\nUsa l'opzione -ignore-size per cambiare il limite\n",
			fileName, ignoreSize)
		return false
	}
	return true
}

func usage() {
	fmt.Fprint(flag.CommandLine.Output(), "Funzionamento del programma:\n")
	fmt.Println("Questo programma prende in input un file audio, lo divide a ogni pausa e scrive il risultato" +
		" su più file di output. Per funzionare, necessita che sul sistema sia installato Sox http://sox.sourceforge.net/" +
		" e che si abbia un token di accesso per Wit https://wit.ai/docs/quickstart\n" +
		"Il programma ti chiederà di inserire il token la prima volta che lo avvii e lo salverà in un file wittoken.json" +
		". In alternativa, è possibile passarlo come argomento con la flag -token, che ha la precedenza su quello " +
		"contenuto nel file. Infine, verrà messo in un file txt il risultato dello speech to text. Ricorda che Wit " +
		"non supporta audio più lunghi di 20 secondi, per cui se uno dei file di output è più lungo, non sarà possibile " +
		"averne la trascrizione.")
	advancedCommands := map[string]bool{"aft-split": true, "after-duration": true, "bef-split": true, "before-duration": true,
		"keep-pause": true, "threshold": true}
	advancedQueue := make(chan *flag.Flag, len(advancedCommands))
	flag.CommandLine.VisitAll(func(f *flag.Flag) {
		if _, ok := advancedCommands[f.Name]; ok {
			advancedQueue <- f
			return
		}
		printFlag(f)
	})
	close(advancedQueue)

	fmt.Println("Opzioni avanzate (per SoX):")
	for f := range advancedQueue {
		printFlag(f)
	}
}

func printFlag(f *flag.Flag) {
	s := fmt.Sprintf("  -%s", f.Name) // Two spaces before -; see next two comments.
	name, usage := flag.UnquoteUsage(f)
	if len(name) > 0 {
		s += " " + name
	}
	// Boolean flags of one ASCII letter are so common we
	// treat them specially, putting their usage on the same line.
	if len(s) <= 4 { // space, space, '-', 'x'.
		s += "\t"
	} else {
		// Four spaces before the tab triggers good alignment
		// for both 4- and 8-space tab stops.
		s += "\n    \t"
	}
	s += strings.ReplaceAll(usage, "\n", "\n    \t")

	fmt.Fprint(flag.CommandLine.Output(), s, "\n")
}